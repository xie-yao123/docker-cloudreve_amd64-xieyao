# 来自作者的废话

[Cloudreve ](https://github.com/cloudreve/cloudreve)

[Gitee](https://gitee.com/xie-yao123/projects)

[linux安装docker](https://user.qzone.qq.com/1297754537/main)


### 准备

- Docker :  国内 Linux 一键安装docker :  curl -s https://get.docker.com/ | sudo sh
- git : sentos系统自动安装 git :  sudo yum install git

### 使用步骤

#### 快速下载集成环境
git clone https://gitee.com/xie-yao123/docker-cloudreve_amd64-xieyao.git

#### 给文件增加权限 - 根据执行上面命令时所在的目录是否需要 [ ./  ]
chmod -R 777 ./docker-cloudreve_amd64-xieyao/install-shell.sh

#### 安装shell命令。注：用于后续遥控容器内的程序运行等！
./docker-cloudreve_amd64-xieyao/install-shell.sh

#### 将 git 的文件执行成 Docker 镜像。 注：[1]：想要的docker镜像名 ；[2]：Dockerfile文件所在目录。
sudo docker build -t [1] [2]

#### 检查是否有容器存在
docker images

#### 将上一步的镜像执行为容器。注：一条一条的复制执行！

```bash
docker run -d \
--name own \
-p 83:83 \
-v /root/own/log:/core/log \
-v /root/own/uploads:/core/uploads \
-v /root/own/db:/core/db \
-v /root/own/etc:/core/etc \
<镜像文件的全名>

> 解释：
# -p 端口的映射外部83，内部83
# -v 端口映射
# -d 后台运行
# --name own 这个我给这个容器起的是own这个名字,这个名字很重要,下面的操作已经使用
# --link <ip|域名|其他容器名>:<容器内host>
# eg:
# --link localhost:roothost 这里将127.0.0.1映射到容器内roothost这个名字，可以ping roothost来测试
```
#### 检查是否有容器存在
docker ps 或docker ps -a 

#### 开机自启动
> 这个功能，shell暂时不支持

```bash
systemctl enable docker
docker update --restart=always own
```

##  重点 ！！
>上一步执行完后一定要看一下日志： 包含初始账号密码!!!

#### 这里存储着初始账号密码的日志文件
cat /root/own/log/cloudreve.log

#### 这里存储着离线下载的日志文件
cat /root/own/log/aria2.log

#### 进入初始之后，需要第一次进入修改个人配置。
例如：用户名；密码；存储空间；常用登录地址。

#### 访问方式：
> 其他设备：
ip a  -->0.0.0.0:83
> 本地设备：127.0.0.1:83

- RPC 服务器地址：http://localhost:6800
- RPC Secret：ownaria2
- 临时下载目录：/tmp

速度不快的话自己进容器添加aria2 的tacker 服务器 加速

### 宿主与容器内文件映射关系

| 容器内的数据卷   | 说明                                                               |
| ---------------- | ------------------------------------------------------------------ |
| /core/uploads    | 存储用户上传的文件                                                 |
| /core/log        | 存储 aria2 和 cloudreve 的日志                                     |
| /core/etc        | 存储 cloudreve 的配置                                              |
| /core/db         | 存储数据库文件，这个项依赖于配置文件`conf.ini`里[Database]->DBFile |
| /core/aria2/conf | aria2 的配置文件，不建议修改（除非你会）                           |


```
### 备份

```bash
mkdir /root/backup
cp /root/own/db /root/backup
cp /root/own/uploads /root/backup
```

### 修改配置文件

```bash
vi /root/own/etc/conf.ini
```

# 最后-简单直接操作！
> 谢瑶 QQ : 1297754537 ，作者已经封装为docker hub 镜像  直接下载使用！
#### 安装好docker后，执行：docker pull xy1297754537/cloudreve

### 查看镜像
docker images

### 将镜像运行为容器
```
docker run -d \
--name own \
-p 83:83\
-v /root/own/log:/core/log \
-v /root/own/uploads:/core/uploads \
-v /root/own/db:/core/db \
-v /root/own/etc:/core/etc \
    xy1297754537/cloudreve （刚刚pull的镜像全名）
```

### 查看容器以及状态
docker ps -a

### 通过shell命令遥控容器内的 cloudreve 程序 （ 注 ：暂不支持shell 开机启动，需要进入容器内操作 ）
#### 启动  :    docker start own
#### 重启  :    docker restart own
#### 关闭  :    docker stop own
#### 更新  :     cloudreve-update  （我会一直关注更新docker hub 此条不重要）
> 目前 cloudreve 最新 版本为 3.5.2

### 今天看到有人用我的docker 镜像了 所以今天更新一下cloudreve和aria2主程序







